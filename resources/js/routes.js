import UserEdit from './components/user/edit';
import UsersIndex from './components/user/index';

export const routes = [
    {
        path: '/user/edit/:userId',
        component: UserEdit,
        name: "UserEdit"
    },
    {
        path: '/',
        component: UsersIndex,
        name: "UsersIndex"
    },
];
