#ГАЙД УСТАНОВКИ ПРОЕКТА ЛОКАЛЬНО:
___________________________________________________
    #1. Клонируем проект 

    #2. Создаем пустую БД (utf8mb4_unicode_ci)
    
    #3. Создаем .env файл и копируем в него содержимое файла .env.example
    
    #4. Заменяем в нем на свои значения DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD

    #5. composer install

    #6. npm install

    #7. npm run dev 

    #8. php artisan key:generate

    #9. php artisan migrate:fresh --seed
    
    #10 php artisan serve
