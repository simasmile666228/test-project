<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
                      [
                          'name'              => 'Admin',
                          'email'             => 'admin@gmail.com',
                          'phone'             => '380990083456',
                          'password'          => Hash::make('112233'),
                          'email_verified_at' => now(),
                          'created_at'        => now(),
                          'updated_at'        => now()
                      ],
                      [
                          'name'              => 'User123',
                          'email'             => 'user@gmail.com',
                          'phone'             => '3809900234456',
                          'password'          => Hash::make('123123'),
                          'email_verified_at' => now(),
                          'created_at'        => now(),
                          'updated_at'        => now()
                      ],
                      [
                          'name'              => 'User1234',
                          'email'             => 'user13@gmail.com',
                          'phone'             => '3809902343456',
                          'password'          => Hash::make('123123'),
                          'email_verified_at' => now(),
                          'created_at'        => now(),
                          'updated_at'        => now()
                      ],
                  ];
        foreach($users as $user){
            User::create($user);
        }
    }
}
