<?php

namespace App\Http\Services;

use App\Http\Repositories\UserRepository;

class UserService
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAllUsers()
    {
        return $this->userRepository->all();
    }

    public function getUserById($userId)
    {
        return $this->userRepository->getById($userId);
    }

    public function update($userId, $userParams)
    {
        return $this->userRepository->update($userId, $userParams);
    }
}
