<?php

namespace App\Http\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $eloquent;

    /**
     * @return mixed
     * @throws \Exception
     */
    protected function model()
    {
        if (!$this->eloquent) {
            throw new \Exception('No eloquent set for ' . get_class($this));
        }
        return new $this->eloquent;
    }

  /**
   * @throws \Exception
   */
    public function all(): Collection
    {
        return $this->model()->all();
    }

    public function firstOrDefault($id)
    {
        return $this->model()->firstOrDefault($id);
    }

    public function findOrDefault($id)
    {
        return $this->model()->findOrDefault($id);
    }

    public function findOrFail($id)
    {
        return $this->model()->findOrFail($id);
    }

    public function delete(int $id): bool
    {
        return (bool) $this->model()->findOrFail($id)->delete();
    }
}
