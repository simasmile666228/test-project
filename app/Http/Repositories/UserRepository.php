<?php

namespace App\Http\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    protected $eloquent = User::class;

    public function getById($id)
    {
        return $this->model()->where('id', '=', $id)->first();
    }

    public function update(int $id, array $params)
    {
        $user = $this->getById($id);
        return $user->update($params);
    }
}
