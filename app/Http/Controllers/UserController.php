<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Services\UserService;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
         return response()->json([
            'users' => $this->userService->getAllUsers()
         ], 200);
    }

    public function edit($userId)
    {
        return response()->json([
            'user' => $this->userService->getUserById($userId)
        ], 200);
    }

    public function update($userId, UserRequest $request)
    {
        $result = $this->userService->update($userId, $request->except(['_token', '_method']));
        return $result ? response()->json(['status' => true], 200) : response()->json(['status' => false], 400);
    }
}
